import express from "express";
import getRedisClient from "./db/connection.js";

const app = express();

app.use(express.json());

const db = getRedisClient();
const port = process.env.PORT || 3000;

app.get("/message", async (_, res) => {
  const message = await db.get("message");
  res.json({ message });
});

app.post("/message", async (req, res) => {
  const { content } = req.body;

  if (!content) return res.status(400).json({ error: "Content is required" });

  const messages = await db.set("message", content);
  res.status(201).json(messages);
});

app.listen(port, async () => {
  await db.connect();
  console.log(`Server is running on port ${port}`);
});
