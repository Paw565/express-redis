import { createClient } from "redis";

const redisUrl = process.env.REDIS_URL || "redis://localhost:6379";

const _redisClient = await createClient({
  url: redisUrl,
}).on("error", (err) => {
  console.error("Redis Client Error", err);
  process.exit(1);
});

const getRedisClient = () => _redisClient;

export default getRedisClient;
