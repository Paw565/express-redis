FROM node:lts-alpine

WORKDIR /server

RUN corepack enable

COPY package.json pnpm-lock.yaml ./

RUN pnpm --frozen-lockfile i

COPY . .

EXPOSE 3000

CMD pnpm start
